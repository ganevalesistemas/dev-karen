import {Component, ViewChild} from '@angular/core';
import {Screenshot} from '@ionic-native/screenshot/ngx';
import {SocialSharing} from '@ionic-native/social-sharing';
import {
  IonicPage,
  App,
  NavController,
  NavParams,
  Content,
  Platform,
  AlertController
} from 'ionic-angular';
import {Voucher} from '../../models/voucher';
import {User} from "../../models/user";
import {UtilsProvider} from "../../providers/utils/utils";
import {UserProvider,VoucherProvider} from "../../providers";

/**
 * Generated class for the ReceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-receipt',
    templateUrl: 'receipt.html',
})
export class ReceiptPage {
  @ViewChild(Content) content: Content;
  date: Date;
  voucher: Voucher;
  screen: any;
  state: boolean = false;
  user: User;
  day: Date;
  previous: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private screenshot: Screenshot,
              private socialSharing: SocialSharing,
              private utilsProvider: UtilsProvider,
              private userProvider: UserProvider,
              private voucherProvider: VoucherProvider,
              private app: App,
              public alertCtrl: AlertController,
              public platform: Platform) {
    this.date = navParams.get("date");
    this.voucher = navParams.get("voucher");
    this.user = navParams.get("user");
    this.day = navParams.get("day");
    this.previous = navParams.get("previous");
  }

  ionViewCanEnter(): boolean {
    console.log("checking if can enter page");
    const allowed = this.userProvider.isLoggedIn();
    if (!allowed) {
        setTimeout(() => {
            this.utilsProvider.showToast('Tu sesión ha expirado, por favor ingresa de nuevo', 3500);
            this.app.getRootNav().setRoot('WelcomePage');
        });
    }
    return allowed;
  }

  // Metodo que obtiene el saldo restante
  // Total de los installments pagados - el total de la venta
  // Estas cantidades ya tienen PDS
  payment(totalPay,totalAmount){
    var totalPayment: number;

    totalPayment = (parseFloat(totalAmount.replace(/,/g,""))) - (parseFloat(totalPay.replace(/,/g,"")));

    return totalPayment.toFixed(0);
  }

  balance(amount){
    var totalPayment: number;

    totalPayment = (parseFloat(this.voucher.totalAmount.replace(/,/g,""))) - (parseFloat(this.voucher.totalPaid.replace(/,/g,"")));

    totalPayment = totalPayment + amount;

    return totalPayment.toFixed(0);
  }

  //Metodo para sacar el installment en el que va
  number(){
    var number: number;

    number = this.voucher.totalInstallments - (this.voucher.installments.owed.length);

    return number;
  }


  // Metodo que obtiene el saldo restante
  // Total de los installments pagados - el total de la venta
  // Estas cantidades ya tienen PDS
  paymentOwed(totalPay, amount){
    var totalPayment: number;

    // totalPayment = ((parseFloat(amount.replace(/,/g,""))) - (parseFloat(totalPay.replace(/,/g,"")))) - amount ;

    totalPayment = totalPay - amount ;

    return totalPayment.toFixed(0);
  }

  balanceOwed(){
    var totalPayment: number;

    totalPayment = (parseFloat(this.voucher.totalAmount.replace(/,/g,""))) - (parseFloat(this.voucher.totalPaid.replace(/,/g,"")));

    return totalPayment.toFixed(0);
  }

  //Metodo para sacar el installment en el que va
  numberOwed(){
    var number: number;

    number = (this.voucher.totalInstallments - (this.voucher.installments.owed.length) + 1);

    return number;
  }

  //Muetsra una notificacion de que el vale se ha guardado con exito
  //Regresa a la pantalla principal
  reset() {
    var self = this;
    this.voucherProvider.saveReceipt({voucherId: this.voucher.id,distributorId: this.voucher.distributorId});
    let alert = this.alertCtrl.create({
      title: 'Has enviado un Recibo Electrónico!'
    });
    alert.present();
    setTimeout(function(){
      self.state = false;
      alert.dismiss();
      self.app.getRootNav().setRoot('WelcomePage');
    }, 1000);
  }

  // Metodo que guarda la imagen en el celular
  // por el nombre de la imagen solo se puede tener 1 imagen del mismo vale en el celular
  screenShot() {
    this.state = true;
    this.screenshot.save('jpg', 80, ""+ this.voucher.id + "").then(res => {
      this.state = true;
      this.screen = res.filePath;
      this.state = true;
      this.reset();
    });
  }

  //Metodo para mandar la imagen por whats
  whatsShare() {
    this.state = true;
    this.platform.ready().then(() => {
      this.screenshot.URI(80).then(res => {
        this.socialSharing.shareViaWhatsApp(null, res.URI, null)
          .then(() => {},
            () => {
              alert('SocialSharing failed');
            });
        this.state = true;
        this.screen = res.filePath;
        this.state = true;
        this.reset();
      });
    });
  }



}
